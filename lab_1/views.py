from django.shortcuts import render
from datetime import datetime, date
import os
# Enter your name here
mhs_name = 'Muhammad Rizqi Agung Prabowo' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1997, 11, 28) #TODO Implement this, format (Year, Month, Date)
npm = 1706074801 # TODO Implement this
univ = 'Universitas Indonesia'
hobby = 'futsal'
bio = 'i sleep most of the time'
imgURL =  os.path.dirname(os.path.dirname(__file__))
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'univ': univ, 'hobby': hobby, 'bio': bio, 'imgURL': imgURL}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
